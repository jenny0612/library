<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/library','LibraryController@index');
Route::get('/library/tambah','LibraryController@tambah');
Route::post('/library/store','LibraryController@store');
Route::get('/library/edit/{id}','LibraryController@edit');
Route::post('/library/update','LibraryController@update');
Route::get('/library/hapus/{id}','LibraryController@hapus');

Route::get('/library/categori', 'LibraryController@index');
