<!DOCTYPE html>
<html>
<head>
	<title>Relasi One To Many Eloquent</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
 
	<div class="container">
		<div class="card mt-5">
			<div class="card-body">
				<h3 class="text-center"><a href="https://www.malasngoding.com">LIBRARY FOR US</a></h3>
				<h5 class="text-center my-4">Eloquent One To Many Relationship</h5>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Kategori</th>
							<th>Judul Buku</th>
							<th width="15%" class="text-center">Jumlah Buku</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categori as $c)
						<tr>
							<td>{{ $a->judul }}</td>
							<td>
								@foreach($a->library as $l)
									{{$l->library}},
								@endforeach
							</td>
							<td class="text-center">{{ $a->library->count() }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
 
</body>
</html>