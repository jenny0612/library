<!DOCTYPE html>
<html>
<head>
	<title>Library</title>
</head>
<body>

	<style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style>

 
	<h2>LIBRARY FOR US</h2>
	<h3>Daftar Buku</h3>
 
	<a href="/library/tambah"> + Tambah Buku Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Judul Buku</th>
			<th>Kategori</th>
			<th>Pengarang</th>
            <th>Tahun Terbit</th>
			<th>Action</th>
		</tr>
		@foreach($library as $l)
		<tr>
            <td>{{ $l->id }}</td>
			<td>{{ $l->JudulBuku }}</td>
			<td>{{ $l->kategori }}</td>
			<td>{{ $l->pengarang }}</td>
			<td>{{ $l->tahun_terbit }}</td>
			<td>
				<a href="/library/edit/{{ $l->id }}">Edit</a>
				|
				<a onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')" href="/library/hapus/{{ $l->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
	<br>
	Click here for see:  <a href="https://www.malasngoding.com">Eloquent One to Many</a>
	{{ $library->links() }}
 
</body>
</html>