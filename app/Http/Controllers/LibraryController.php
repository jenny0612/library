<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\categori;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$library = Library::all();
        //return view('library',['library' => $library]);

        // mengambil data dari table library
        $library = DB::table('library')->paginate(6);
 
    	// mengirim data library ke view index
    	return view('index',['library' => $library]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // insert data ke table library
	DB::table('library')->insert([
		'JudulBuku' => $request->JudulBuku,
		'kategori' => $request->kategori,
        'pengarang' => $request->pengarang,
        'tahun_terbit' => $request->tahun_terbit
	]);
	// alihkan halaman ke halaman library
	return redirect('/library');
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // method untuk menampilkan view form tambah daftar buku
    public function tambah()
    {
 
	    // memanggil view tambah
	    return view('tambah');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // mengambil data library berdasarkan id yang dipilih
	    $library = DB::table('library')->where('id',$id)->get();
	    // passing data library yang didapat ke view edit.blade.php
	    return view('edit',['library' => $library]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // update data library
    	DB::table('library')->where('id',$request->id)->update([
            'JudulBuku' => $request->JudulBuku,
		    'kategori' => $request->kategori,
            'pengarang' => $request->pengarang,
            'tahun_terbit' => $request->tahun_terbit
    	]);
    	// alihkan halaman ke halaman library
    	return redirect('/library');
    }

    // method untuk hapus data library
    public function hapus($id)
    {
    	// menghapus data library berdasarkan id yang dipilih
    	DB::table('library')->where('id',$id)->delete();
		
    	// alihkan halaman ke halaman pegawai
    	return redirect('/library');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
